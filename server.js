/**************************************************************************/
/*                BACKEND PROYECTO BOOTCAMP CHEMA ed. 4                   */
/**************************************************************************/

/*Incluimos la libreria express y el body-parser para poder tratar los
body en las peticiones REST ya que node las trata directamente       */

var express = require ('express');
var app = express();

/*Incluimos las siguientes lineas para poder recoger el body de las
peticiones y que se parsee correctamente                           */

var bodyParser = require('body-parser');
app.use(bodyParser.json());

// Definimos el puerto por el que vamos a recibir las peticiones HTTP
var port = process.env.PORT || 3000;

//Incluimos el cliente http request-json
var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu_chema/collections/";
var mLabAPIKey = "apiKey=yUqq0VK1zZUjjYE_ZWbyQafuFyz-fJpV";
var requestJson=require('request-json');


//Ponemos el servidor a escuchar
app.listen(port);
console.log("API escuchando en el puerto BIP "+port);


/* Definimos una URL con GET para responder a las peticiones GET y comprobar
que el back esta listo. localhost:3000/apitechu/v1 */

app.get('/apitechu/v1',
  function(req,res){
    res.send(
      {
        "msg":"Bienvenido a la API de Chema en la Tech University ed4"
      }
    )
  }
);

/**************************************************************************/
/* Servicios usados en el front del proyecto                              */
/**************************************************************************/

//Obtencion de todos los usuarios
app.get('/apitechu/v2/users',
  function (req,res){

    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("user?"+ mLabAPIKey,
    function(err, resMLab,body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
      }
    )
  }
);

//Obtencion de usuario por id
app.get('/apitechu/v2/users/:id',
  function (req,res){
    //Creamos la consulta con el id recibido
    var id = req.params.id;
    var query = 'q={"id" : '+  id + '}';
    //Generamos el cliente con la consulta http
    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

/*Definimos URL para login de usuarios*/

app.post('/apitechu/v2/login',
  function (req,res){
  //Recogemos los datos de Body. User,Pwd,email
      var loginUser ={
          "pwd" : req.body.pwd,
          "email" : req.body.email
      }
    //Buscamos el mail en la lista de usuarios para ver si existe
    //Si existe le logamos y sino devolvemos error
    var query = 'q={"email" : "'+  loginUser.email +
                  '", "pwd" : "' + loginUser.pwd + '"}';

    //Generamos el cliente con la consulta http

    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("user?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = JSON.parse('{"id" : '+  body[0].id + ', "first_name" : "' +  body[0].first_name + '", "last_name" : "' +  body[0].last_name + '"}') ;
             //Hacemos put para hacer logged = true
             var query = 'q={"id" : '+  body[0].id + '}';
   					 var putBody = '{"$set" : {"logged" : true}}';
   					 httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
   								  function (errPUT, resMLabPUT, bodyPUT){
   									   if (errPUT) {
   										 response = {"msg" : "Error logando usuario."}
   										 res.status(500);
   									   } else {
   										 if (bodyPUT.length > 0) {
                         response = JSON.parse('{"id" : '+  body[0].id + ', "first_name" : "' +  body[0].first_name + '", "last_name" : "' +  body[0].last_name + '"}') ;
   										 } else {
   										   response = {"msg" : "Usuario a logar no encontrado."};
   										   res.status(404);
   										 }
   									   }
                       res.status(200);
   										}
   									)
                    //Finalizamos el PUT
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

/*Definimos URL para deslogar usuarios*/

app.post('/apitechu/v2/logout',
  function (req,res){
  //Recogemos los datos de Body. id
      var logoutUser ={
          "id" : req.body.id,
      }
      //Creamos la consulta con el id recibido
      var id = req.body.id;
      var query = 'q={ "$and" :   [ {"id":' + id + '},{"logged": true } ] }';

      //Generamos el cliente con la consulta http
      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("user?"+ query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error obteniendo usuario."
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = body[0];
             } else {
               response = {
                 "msg" : "Usuario no encontrado."
               };
               res.status(404);
             }
           }
           //Hacemos put para eliminar logged = true
           //Solo si la query anterior ha obtenido resultado
          if (body.length > 0) {
                     var query = 'q={"id" : '+  body[0].id + '}';
                      var putBody = '{"$unset" : {"logged" : ""}}';
                      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                             function (errPUT, resMLabPUT, bodyPUT){
                                if (errPUT) {
                                response = {"msg" : "Error deslogando usuario."}
                                res.status(500);
                                }
                                else {
                                if (bodyPUT.length > 0) {
                                  response = body[0];
                                } else {
                                  response = {"msg" : "Usuario a deslogar no encontrado."};
                                  res.status(404);
                                }
                                }
                                res.status(200);
                               }
                             )
            //Finalizamos el PUT
          }
          res.send("Logout Finalizado");
         }
      )
    }
  );

  /*Definimos URL para creacion de usuarios*/

  app.post('/apitechu/v2/newuser',
    function (req,res){
      response=null;
      //Comprobamos si el usuario que se quiere dar de alta ya existe
      // a través del email.
      var query = 'q={"email" : "'+  req.body.email + '"}';
      //Generamos el cliente con la consulta http
      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("user?"+ query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error consultando email para creación de usuario."
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = {
                 "msg" : "Usuario ya existente."
               }
              res.status(409);
             }
             else {
                var query = '&s={id:-1}&l=1';
                httpClient.get("user?"+ query + "&" + mLabAPIKey,
                function(err, resMLab, body) {
                     if (err) {
                       response = {
                         "msg" : "Error obteniendo ultimo id."
                       }
                       res.status(500);
                     } else {
                       //Construimos el documento a insertar
                       var postBody = {
                           "id" : body[0].id + 1,
                           "first_name" : req.body.first_name,
                           "last_name" : req.body.last_name,
                           "pwd" : req.body.pwd,
                           "email" : req.body.email
                       }
                       httpClient = requestJson.createClient(baseMlabURL);
                       httpClient.post("user?"+ "&" + mLabAPIKey,  postBody,
                       function(err, resMLab, body) {
                            if (err) {
                              response = {
                                "msg" : "Error creando usuario."
                              }
                              res.status(500);
                            } else {
                              response = {
                                "msg" : "Usuario Creado."
                              }
                              res.status(200);
                            }
                          }
                       )
                     }
                   }
                )
             }
           }
           res.send(response);
         }
      )
    }
);

/*Definimos URL para generación de numero de cuenta*/

app.get('/apitechu/v2/newaccount',
  function (req,res){
    response=null;
    //Generamos numero de cuenta que no exista y lo devolvemos
    do{
      var control=false;
      //Generamos numero de cuenta
      var numCuenta = "ES32";
      for(var i = 1; i < 6; i++){
        var aleatorio = Math.round(Math.random()*9999);
        numCuenta += ' ' + aleatorio
      }
      //Comprobamos si la cuenta que se quiere dar de alta ya existe
      var query = 'q={"iban" : "'+  numCuenta + '"}';
      //Generamos el cliente con la consulta http
      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account?"+ query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
           if (err) {
           } else {
             if (body.length > 0) {
               response = {
                 "msg" : "Cuenta ya existente"
               }
             }
             else {
               response = {
                 "msg" : numCuenta
               }
               control=true;
             }
           }
         res.send(response);
         }
      )
    }while (control);
  }
);

/*Definimos URL para creacion de cuentas de usuario*/

app.post('/apitechu/v2/newuseraccount',
  function (req,res){
    response=null;
   //Creamos la cuenta
   //Construimos el documento a insertar
   var postBody = {
       "userid" : req.body.userid,
       "iban" : req.body.iban,
       "balance" : req.body.balance
   }
   httpClient = requestJson.createClient(baseMlabURL);
   httpClient.post("account?"+ "&" + mLabAPIKey,  postBody,
   function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error creando Cuenta."
          }
          res.status(500);
        } else {
          response = {
            "msg" : "Cuenta Creada."
          }
          res.status(200);
        }
        res.send(response);
      }
   )
  }
);

//Obtencion de cuentas por id de usuario
  app.get('/apitechu/v2/users/:id/accounts',
    function (req,res){
      //Creamos la consulta con el id recibido
      var id = req.params.id;
      var query = 'q={"userid" : '+  id + '}';
      //Generamos el cliente con la consulta http
      httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account?"+ query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error obteniendo cuentas."
             }
             res.status(500);
           } else {
             if (body.length > 0) {
               response = body;
             } else {
               response = {
                 "msg" : "Cuentas no encontradas."
               };
               res.status(404);
             }
           }
           res.send(response);
         }
      )
    }
  );


//Obtencion de movimientos por id de cuenta
app.get('/apitechu/v2/accounts/:id/movements',
  function (req,res){
    //Creamos la consulta con el id recibido
    var id = req.params.id;
    var query = 'q={"account_id" : "'+  id + '"}';
    //Generamos el cliente con la consulta http
    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("movements?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo movimientos."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body;
           } else {
             response = {
               "msg" : "Movimientos no encontradas."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
    )
  }
);

/*Definimos URL para ingreso de efectivo*/
app.post('/apitechu/v2/accounts/cashincome',
  function (req,res){
    var response=null;
    //Obtenemos los datos de la cuenta sobre la que realizar el ingreso
    // a través del id recibido.
    var query = 'q={"_id" : {"$oid":"'+  req.body.accountid + '"}}';
    //Generamos el cliente con la consulta http
    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("account?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error consultando cuenta para realizar ingreso."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             //Hacemos put para sumar el ingreso al balance
             var accountUserid = body[0].userid;
             var newBalance = body[0].balance + req.body.amount;
             var query = 'q={"_id" : {"$oid":"'+  req.body.accountid + '"}}';
   					 var putBody = '{"$set" : {"balance" :' + newBalance + '}}';
   					 httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
   								  function (errPUT, resMLabPUT, bodyPUT){
   									   if (errPUT) {
   										 response = {"msg" : "Error aumentando balance."}
   										 res.status(500);
   									   } else {
                         var putResponse = JSON.stringify(bodyPUT);
     										 if (bodyPUT.n == 1) {
                           var query = 'q={"account_id" : "'+  req.body.accountid + '","user_id":' + accountUserid + '}&s={move_id:-1}&l=1';
                           httpClient.get("movements?"+ query + "&" + mLabAPIKey,
                           function(err, resMLab, body) {
                                if (err) {
                                  response = {
                                    "msg" : "Error obteniendo ultimo movimiento de cuenta."
                                  }
                                  res.status(500);
                                } else {
                                  if (body[0]==undefined){
                                    var resMove=0;
                                  }else{
                                    var resMove=body[0].move_id;
                                  }
                                  //construimos fecha actual
                                  var day = new Date(Date.now()).getDate();
                                  var month = new Date(Date.now()).getMonth()+1;
                                  var year = new Date(Date.now()).getFullYear();
                                  var actualDate = day + "/" + month + "/" + year;
                                  //Construimos el documento a insertar
                                  var postBody = {
                                      "account_id" : req.body.accountid,
                                      "user_id" : accountUserid,
                                      "move_id" : resMove + 1,
                                      "date" : actualDate,
                                      "amount" : req.body.amount,
                                      "type" : "1"
                                  }
                                  httpClient.post("movements?"+ "&" + mLabAPIKey,  postBody,
                                  function(err, resMLab, body) {
                                       if (err) {
                                         response = {
                                           "msg" : "Error creando movimiento."
                                         }
                                         res.status(500);
                                       } else {
                                         response = {
                                           "msg" : "Movimiento Creado."
                                         }
                                         res.status(200);
                                       }
                                     }
                                  )
                                }
                              }
                           )
     										 } else {
     										   response = {"msg" : "Cuenta para aumentar el balance no encontrada."};
     										   res.status(404);
     										 }
   									   }
   										}
   									)
           }
           else {
              console.log ("No hemos encontrado la cuenta sobre la que realizar el ingreso");
           }
         }
         response = {
           "msg" : "Ingreso Finalizado"
         }
         res.send(response);
       }
    )
  }
);


/*Definimos URL para retirada de efectivo*/
app.post('/apitechu/v2/accounts/cashwithdrawal',
  function (req,res){
    var response=null;
    //Obtenemos los datos de la cuenta sobre la que realizar la retirada
    // a través del id recibido.
    var query = 'q={"_id" : {"$oid":"'+  req.body.accountid + '"}}';
    //Generamos el cliente con la consulta http
    httpClient = requestJson.createClient(baseMlabURL);
    httpClient.get("account?"+ query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error consultando cuenta para realizar retirada."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             //Hacemos put para restar el importe al balance
             var accountUserid = body[0].userid;
             var newBalance = body[0].balance - req.body.amount;
             var query = 'q={"_id" : {"$oid":"'+  req.body.accountid + '"}}';
   					 var putBody = '{"$set" : {"balance" :' + newBalance + '}}';
   					 httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
   								  function (errPUT, resMLabPUT, bodyPUT){
   									   if (errPUT) {
   										 response = {"msg" : "Error restando balance."}
   										 res.status(500);
   									   } else {
                         var putResponse = JSON.stringify(bodyPUT);
     										 if (bodyPUT.n == 1) {
                           var query = 'q={"account_id" : "'+  req.body.accountid + '","user_id":' + accountUserid + '}&s={move_id:-1}&l=1';
                           httpClient.get("movements?"+ query + "&" + mLabAPIKey,
                           function(err, resMLab, body) {
                                if (err) {
                                  response = {
                                    "msg" : "Error obteniendo ultimo movimiento de cuenta."
                                  }
                                  res.status(500);
                                } else {
                                  if (body[0]==undefined){
                                    var resMove=0;
                                  }else{
                                    var resMove=body[0].move_id;
                                  }
                                  //construimos fecha actual
                                  var day = new Date(Date.now()).getDate();
                                  var month = new Date(Date.now()).getMonth()+1;
                                  var year = new Date(Date.now()).getFullYear();
                                  var actualDate = day + "/" + month + "/" + year;
                                  //Construimos el documento a insertar
                                  var postBody = {
                                      "account_id" : req.body.accountid,
                                      "user_id" : accountUserid,
                                      "move_id" : resMove + 1,
                                      "date" : actualDate,
                                      "amount" : req.body.amount,
                                      "type" : "2"
                                  }
                                  httpClient.post("movements?"+ "&" + mLabAPIKey,  postBody,
                                  function(err, resMLab, body) {
                                       if (err) {
                                         response = {
                                           "msg" : "Error creando movimiento."
                                         }
                                         res.status(500);
                                       } else {
                                         response = {
                                           "msg" : "Movimiento Creado."
                                         }
                                         res.status(200);
                                       }
                                     }
                                  )
                                }
                              }
                           )
     										 } else {
     										   response = {"msg" : "Cuenta para restar el balance no encontrada."};
     										   res.status(404);
     										 }
   									   }
   										}
   									)
           }
           else {
              console.log ("No hemos encontrado la cuenta sobre la que realizar la retirada");
           }
         }
         response = {
           "msg" : "Retirada Finalizada"
         }
         res.send(response);
       }
    )
  }
);
