mocha = require('mocha');
chai = require('chai');
chaihttp = require('chai-http');

var server = require('../server.js');

chai.use(chaihttp);
//Asignamos a should la referencia al metodo chai.should para usarlo después
var should = chai.should();


// Creamos un test para probar que la URL indicada devuelve un 200 con una
//aserción
describe ('First test',

  function() {
    it('Test that DuckDuckGo works', function(done){
      chai.request('http://duckduckgo.com')
      .get('/')
      .end(
        function(err,res){
          console.log('Request has ended');
          //console.log(res);
          console.log(err);
          res.should.have.status(200);
          done();
        }
      );
    });
  }
);

//Creamos un caso para comprobar nuestra API en el servidor local, en concreto
//el primer método get que creamos
describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("Bienvenido a la API de Chema en la Tech University ed4")
             done();
           }
         )
     }
   ),

//Comprobamos que todos los objetos de tipo usuario tienen email y password
//devueltos por el verbo GET de nuestro API pidiendo users
  //  it('Prueba que la API devuelve una lista de usuarios correctos.',
  //       function(done) {
  //         chai.request('http://localhost:3000')
  //         .get('/apitechu/v1/users')
  //         .end(
  //           function(err, res) {
  //             res.should.have.status(200);
  //             res.body.should.be.a("array");
  //             for (user of res.body) {
  //               user.should.have.property('email');
  //               user.should.have.property('pwd');
  //             }
  //             done();
  //           }
  //         )
  //       }
  //     )

 }
);
