#Imagen raíz
FROM node

#Carpeta raíz
WORKDIR /apitechu

#Copia de archivos Copia lo que haya en la carpeta de mi proyecto en la
#carpeta apitechu del destino
ADD . /apitechu

#Añadimos un volumen
#VOLUME ['/logs']

#Exponer puerto
EXPOSE 3000

#Instalar dependencias. En nuestro caso no incluimos la carpeta node_modules
#para hacer la imagen mas ligera y que se instale en el momento de despliegue
RUN npm install

#Comando de inicializacion. Mismo que ejecutamos nosotros para dejar el servidor
#disponible
CMD ["npm", "start"]
